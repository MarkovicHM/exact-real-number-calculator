import dec

def decToBinS(deci):
	x = (0,0)
	y = (0,0)
	minx = (0,0)
	maxx = (1,0)
	c = 1
	bina = []
	while(len(deci)-c+1>0):
		o = True
		d = dec.fromIntD(deci[c-1])
		x,y = get_digits(d,minx,maxx)
		while(o != 3):
			o = get_interval(x, y)
			x, y = resize(x,y,o)
			if(o != 3):
				bina.append(o)
		minx = x
		maxx = y
		c+=1
	return bina


def get_digits(d,minx,maxx):
	x = dec.addD(dec.multD(dec.multD(d,(dec.subD(maxx,minx))),(1,-1)), minx)
	y = dec.addD(dec.multD(dec.multD(dec.addD(d,(1,0)),dec.subD(maxx,minx)),(1,-1)), minx)
	#x = (d)*(maxx-minx)*.1 +minx
	#y = (d+1)*(maxx-minx)*.1 +minx
	return x, y

def get_interval(x, y):
	#if(y<=0.5):
	if(dec.leqD(y,(5,-1))):
		return -1
	#elif(x>=0.25 and y<=0.75):
	elif(dec.geqD(x,(25,-2)) and dec.leqD(y,(75,-2))):
		return 0
	#elif(x>=0.5):
	elif(dec.geqD(x,(5,-1))):
		return 1
	else:
		return 3

def resize(x, y, o):
	if(o != 3):
		if(o == -1):
			x = dec.multD(x,(2,0))
			y = dec.multD(y,(2,0))
		elif(o == 0):
			x = dec.multD(dec.subD(x,(25,-2)),(2,0))
			y = dec.multD(dec.subD(y,(25,-2)),(2,0))
			#x = (x-0.25)*2
			#y = (y-0.25)*2
		else:
			x = dec.multD(dec.subD(x,(5,-1)),(2,0))
			y = dec.multD(dec.subD(y,(5,-1)),(2,0))
			#x = (x-0.5)*2
			#y = (y-0.5)*2

	return x, y





