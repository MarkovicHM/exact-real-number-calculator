import average as av
import sb
import math as m
import BS_Factory as fact
import operations as op



class divByDig(sb.BasicStream):
	def __init__(self,inputS, dig):
		sb.BasicStream.__init__(self)
		self.input = inputS
		self.dig = dig
		self.start = True
		self.buildU = True
		self.log = int(m.floor(m.log(m.fabs(self.dig),2)))
		self.rest = 0
		self.zeros = self.log

	def next(self):
		#emits zeros that are guaranteed based on the digit we are dividing by
		if(self.start):
			if(self.zeros > 0):
				self.zeros -= 1
				return self.emitZero()
			else:
				self.start = False
		#calculates the rest from the first digits of input that were ignored
		#in order to emit the guaranteed zeros
		if(self.buildU):
			ff = self.input.nextn(self.log+1)
			for i in range(len(ff)):
				self.rest *= 2
				self.rest+=ff[i]
			self.buildU = False
		#the procedure for the rest of the digits
		else:
			self.rest *= 2
			self.rest += self.input.next()
		
		#if dividing by positive digit
		if(self.dig>=0):
			if(self.rest>=self.dig):
				self.rest -= self.dig
				return self.emitOne()

			elif(self.rest<=-self.dig):
				self.rest += self.dig
				return self.emitMinusOne()

			else:
				return self.emitZero()
		#dividing by negative digit
		else:
			if(self.rest<=self.dig):
				self.rest -= self.dig
				return self.emitOne()

			elif(self.rest>=-self.dig):
				self.rest += self.dig
				return self.emitMinusOne()

			else:
				return self.emitZero()

#for 313131.. gives out max 990 digits, for 314314... it gives out max 823 digits
class delayedDStoBS(sb.BasicStream):
	def __init__(self,rest):
		sb.BasicStream.__init__(self)
		self.started = False
		self.result = 0
		self.rest = fact.BS_Factory(rest)

	def next(self):
		if(not self.started):
			self.result = DStoBS_f(self.rest.copy())
			self.started = True
			return self.result.next()
		else:
			return self.result.next()

def DStoBS_f(decS):
	a = decS.next()
	b = decS.next()
	c = h.digToBS(10*a+b)
	return o.divOOP(av.AverageStream(c,delayedDStoBS(decS)),50)








