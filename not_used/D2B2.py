def decToBinS(deci):
	x = 0
	y = 0
	minx = 0
	maxx = 1
	c = 1
	bina = []
	while(len(deci)-c+1>0):
		o = True
		d = deci[c-1]
		x,y = get_digits(d,c,minx,maxx)
		while(o != 3):
			o = get_interval(x, y)
			x, y = resize(x,y,o)
			if(o != 3):
				bina.append(o)
		minx = x
		maxx = y
		c+=1
	return bina

def get_digits(d,c,minx,maxx):
	x = (d*(maxx-minx))*.1 + minx
	y = (d+1)*(maxx-minx)*.1 +minx
	return x, y

def get_interval(x, y):
	if(y<=0):
		return -1
	elif(x>=-0.5 and y<=0.5):
		return 0
	elif(x>=0):
		return 1
	else:
		return 3

def resize(x, y, o):
	if(o != 3):
		if(o == -1):
			x = x+1
			y = y+1
		elif(o == 0):
			x = x+0.5
			y = y+0.5
		else:
			x = x
			y = y

	return x, y





