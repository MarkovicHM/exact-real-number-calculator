import dec
import sb
import DecToBin as dtb

class DecimalStream (sb.BasicStream):
	def __init__(self,decimal):
		sb.BasicStream.__init__(self)
		self.d = decimal
		self.d1 = (0,0)
		self.x = (0,0)
		self.y = (0,0)
		self.minx = (0,0)
		self.maxx = (1,0)
		self.cont = False
		self.end = False

	def emitOne(self):
		self.x = dec.multD(dec.subD(self.x,(5,-1)),(2,0))
		self.y = dec.multD(dec.subD(self.y,(5,-1)),(2,0))
		self.minx = self.x
		self.maxx = self.y
		return 1

	def emitZero(self):
		self.x = dec.multD(dec.subD(self.x,(25,-2)),(2,0))
		self.y = dec.multD(dec.subD(self.y,(25,-2)),(2,0))
		self.minx = self.x
		self.maxx = self.y
		return 0

	def emitMinusOne (self):
		self.x = dec.multD(self.x,(2,0))
		self.y = dec.multD(self.y,(2,0))
		self.minx = self.x
		self.maxx = self.y
		return -1

	def next(self):
		if(not self.cont):
			self.d1 = dec.fromIntD(int(str(self.d)[0]))
			self.d = str(self.d)[1:]
			if not self.d:
				self.end = True
			self.x, self.y = dtb.get_digits(self.d1,self.minx,self.maxx)
			self.cont=True

		if(self.cont):
			if(dec.leqD(self.y,(5,-1))):
				self.cont = True
				return self.emitMinusOne()
			elif(dec.geqD(self.x,(25,-2)) and dec.leqD(self.y,(75,-2))):
				self.cont = True
				return self.emitZero()
			elif(dec.geqD(self.x,(5,-1))):
				self.cont = True
				return self.emitOne()
			elif self.end:
				return
			else:
				self.cont = False
				return self.next()





