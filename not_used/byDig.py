import DStoBS_f as f
import BS_Factory as fact
import average as av
import sb

def digToB1(d):
	return "{0:b}".format(d)

def multByDig(binS, dig):
	binDig = digToB1(dig)
	binSF = fact.BS_Factory(binS)
	mult = []
	i = 0
	while(i<len(binDig)):
		if(binDig[i] == '1'):
			mult.append(i+1)
		i+=1

	if(len(mult)>1):
		result = f.divByDig(binSF.copy(),2**mult[len(mult)-1])
		last = mult[0]
		mult = mult[1:-1]
		for j in mult:
			result = add(result,f.divByDig(binSF.copy(),2**j))
		result = av.AverageStream(test.copy(), f.divByDig(binSF.copy(),2**last))
	else:
		result = f.divByDig(binSF.copy(),2**mult[0])


	return result

def add(s1,s2):
	ave = av.AverageStream(s1,s2)
	newS = fact.BS_Factory(ave)
	if (newS.copy().next() == 0):
		return shiftLeftBy(newS.copy(),1)
	else:
		raise Exception('cannot add, number too large')

def shiftLeftBy(input, x):
	a = input.nextn(x)
	return input

class neg(sb.BasicStream):
	def __init__(self,stream):
		sb.BasicStream.__init__(self)
		self.stream = stream
	def next(self):
		a = self.stream.next()
		if(a == -1):
			return self.emitOne()
		elif(a == 0):
			return self.emitZero()
		else:
			return self.emitMinusOne()

def sub(s1,s2):
	s2_n = neg(s2)
	return add(s1,s2_n)

	











