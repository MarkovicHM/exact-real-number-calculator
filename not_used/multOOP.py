import dy
import sb

class multOOP(sb.BasicStream):
	def __init__(self, binS, dig):
		sb.BasicStream.__init__(self)
		self.b = binS
		self.d1 = (0,0)
		self.x = (-dig,3)
		self.y = (dig,3)
		self.cont = False

	def emitOne(self):
		self.x = dy.sub(dy.double(self.x),(1,0))
		self.y = dy.sub(dy.double(self.y),(1,0))
		return 1

	def emitZero(self):
		#print self.x
		self.x = dy.double(self.x)
		self.y = dy.double(self.y)
		#print self.x, 'done'

		return 0

	def emitMinusOne (self):
		self.x = dy.add(dy.double(self.x),(1,0))
		self.y = dy.add(dy.double(self.y),(1,0))

		return -1

	def next(self):

		if(not self.cont):
			self.d1 = self.b.next()
			self.x, self.y = get_digits(self.d1,self.x,self.y)
			self.cont = True

		#print self.x, self.y
		if(self.cont):

			if(dy.leq(self.y,(0,0))):
				#print 'OUT: -1'
				self.cont = True
				return self.emitMinusOne()
			elif(dy.geq(self.x,(-1,1)) and dy.leq(self.y,(1,1))):
				#print 'OUT: 0'
				self.cont = True
				return self.emitZero()
			elif(dy.geq(self.x,(0,0))):
				#print 'OUT: 1'
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

def get_digits(d,minx,maxx):
	#print 'IN:', d
	mid = dy.halve(dy.add(maxx,minx))
	if(d == -1):
		x = minx
		y = mid
	elif(d == 0):
		#print minx, maxx, mid
		x = dy.halve(dy.add(minx,mid))
		y = dy.halve(dy.add(maxx,mid))
	else:
		x = mid
		y = maxx
	return x, y








