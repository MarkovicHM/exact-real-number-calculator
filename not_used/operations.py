import DStoBS_f as f
import BS_Factory as fact
import average as av
import sb

def digToB1(d):
	return "{0:b}".format(d)

def multByDig(binS, dig):
	binDig = digToB1(dig)
	binSF = fact.BS_Factory(binS)
	mult = []
	i = 0
	while(i<len(binDig)):
		if(binDig[i] == '1'):
			mult.append(i+1)
		i+=1

	if(len(mult)>1):
		#result = f.divByDig(binSF.copy(),2**mult[len(mult)-1])
		result = shiftRightBy(binSF.copy(),mult[len(mult)-1])
		last = mult[0]
		mult = mult[1:-1]
		for j in mult:
			result = add(result,shiftRightBy(binSF.copy(),j))
			#result = add(result,f.divByDig(binSF.copy(),2**j))
		result = av.AverageStream(result, shiftRightBy(binSF.copy(),last))
		#result = av.AverageStream(result, f.divByDig(binSF.copy(),2**last))
	else:
		result = shiftRightBy(binSF.copy(), mult[0])
		#result = f.divByDig(binSF.copy(),2**mult[0])

	return result

def add(s1,s2):
	ave = av.AverageStream(s1,s2)
	newS = fact.BS_Factory(ave)
	if (newS.copy().next() == 0):
		return shiftLeftBy(newS.copy(),1)
	else:
		raise Exception('cannot add, number too large')

def shiftLeftBy(input, x):
	a = input.nextn(x)
	return input

class shiftRightBy(sb.BasicStream):
	def __init__(self,stream, x):
		sb.BasicStream.__init__(self)
		self.stream = stream
		self.x = x
	def next(self):
		if (self.x>0):
			self.x -= 1
			return self.emitZero()
		else:
			return self.stream.next()

class neg(sb.BasicStream):
	def __init__(self,stream):
		sb.BasicStream.__init__(self)
		self.stream = stream
	def next(self):
		a = self.stream.next()
		if(a == -1):
			return self.emitOne()
		elif(a == 0):
			return self.emitZero()
		else:
			return self.emitMinusOne()

def sub(s1,s2):
	s2_n = neg(s2)
	return add(s1,s2_n)

import dy
import sb

class divOOP(sb.BasicStream):
	def __init__(self, binS, dig):
		sb.BasicStream.__init__(self)
		self.b = binS
		self.d1 = (0,0)
		self.x = (-1,0)
		self.y = (1,0)
		self.dig = dig
		self.cont = True

	def emitOne(self):
		self.x = dy.sub(dy.double(self.x),(self.dig,0))
		self.y = dy.sub(dy.double(self.y),(self.dig,0))
		return 1

	def emitZero(self):
		self.x = dy.double(self.x)
		self.y = dy.double(self.y)

		return 0

	def emitMinusOne (self):
		self.x = dy.add(dy.double(self.x),(self.dig,0))
		self.y = dy.add(dy.double(self.y),(self.dig,0))

		return -1

	def next(self):

		if(not self.cont):
			self.d1 = self.b.next()
			self.x, self.y = get_digits(self.d1,self.x,self.y)
			self.cont = True

		if(self.cont):

			if(dy.leq(self.y,(0,0))):
				self.cont = True
				return self.emitMinusOne()
			elif(dy.geq(self.x,(-self.dig,1)) and dy.leq(self.y,(self.dig,1))):
				self.cont = True
				return self.emitZero()
			elif(dy.geq(self.x,(0,0))):
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

def get_digits(d,minx,maxx):
	mid = dy.halve(dy.add(maxx,minx))
	if(d == -1):
		x = minx
		y = mid
	elif(d == 0):
		x = dy.halve(dy.add(minx,mid))
		y = dy.halve(dy.add(maxx,mid))
	else:
		x = mid
		y = maxx
	return x, y

	











