import external.dy as dy
import external.sb as sb
import dec
import math as m
import rat

class divByInt1(sb.BasicStream):
	def __init__(self, binS, intg):
		sb.BasicStream.__init__(self)
		self.b = binS
		self.d1 = (0,0)
		self.x = (-1,0)
		self.y = (1,0)
		self.intg = intg
		self.cont = True

	def emitOne(self):
		self.x = dy.sub(dy.double(self.x),(self.intg,0))
		self.y = dy.sub(dy.double(self.y),(self.intg,0))
		return 1

	def emitZero(self):
		self.x = dy.double(self.x)
		self.y = dy.double(self.y)

		return 0

	def emitMinusOne (self):
		self.x = dy.add(dy.double(self.x),(self.intg,0))
		self.y = dy.add(dy.double(self.y),(self.intg,0))

		return -1

	def next(self):

		if(not self.cont):
			self.d1 = self.b.next()
			self.x, self.y = get_digits(self.d1,self.x,self.y)
			self.cont = True

		if(self.cont):

			if(dy.leq(self.y,(0,0))):
				self.cont = True
				return self.emitMinusOne()
			elif(dy.geq(self.x,(-self.intg,1)) and dy.leq(self.y,(self.intg,1))):
				self.cont = True
				return self.emitZero()
			elif(dy.geq(self.x,(0,0))):
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

"""def get_digits(d,minx,maxx):
	mid = dy.halve(dy.add(maxx,minx))
	if(d == -1):
		x = minx
		y = mid
	elif(d == 0):
		x = dy.halve(dy.add(minx,mid))
		y = dy.halve(dy.add(maxx,mid))
	else:
		x = mid
		y = maxx
	return x, y
"""

class divByInt2(sb.BasicStream):
	def __init__(self,inputS, intg):
		sb.BasicStream.__init__(self)
		self.input = inputS
		self.intg = intg
		self.start = True
		self.buildU = True
		self.log = int(m.floor(m.log(m.fabs(self.intg),2)))
		self.rest = 0
		self.zeros = self.log

	def next(self):
		#emits zeros that are guaranteed based on the integer we are dividing by
		if(self.start):
			if(self.zeros > 0):
				self.zeros -= 1
				return self.emitZero()
			else:
				self.start = False
		#calculates the rest from the first digits of input that were ignored
		#in order to emit the guaranteed zeros
		if(self.buildU):
			ff = self.input.nextn(self.log+1)
			for i in range(len(ff)):
				self.rest *= 2
				self.rest+=ff[i]
			self.buildU = False
		#the procedure for the rest of the digits
		else:
			self.rest *= 2
			self.rest += self.input.next()
		
		#if dividing by positive digit
		if(self.intg>=0):
			if(self.rest>=self.intg):
				self.rest -= self.intg
				return self.emitOne()

			elif(self.rest<=-self.intg):
				self.rest += self.intg
				return self.emitMinusOne()

			else:
				return self.emitZero()
		#dividing by negative integer
		else:
			if(self.rest<=self.intg):
				self.rest -= self.intg
				return self.emitOne()

			elif(self.rest>=-self.intg):
				self.rest += self.intg
				return self.emitMinusOne()

			else:
				return self.emitZero()

class multByInt(sb.BasicStream):
	def __init__(self, binS, intg):
		sb.BasicStream.__init__(self)
		self.b = binS
		self.d1 = (0,0)
		self.x = (-m.fabs(intg), int(m.ceil(m.log(m.fabs(intg),2))))
		self.y = (m.fabs(intg), int(m.ceil(m.log(m.fabs(intg),2))))
		self.cont = False

	def emitOne(self):
		self.x = dy.sub(dy.double(self.x),(1,0))
		self.y = dy.sub(dy.double(self.y),(1,0))
		return 1

	def emitZero(self):
		#print self.x
		self.x = dy.double(self.x)
		self.y = dy.double(self.y)
		#print self.x, 'done'

		return 0

	def emitMinusOne (self):
		self.x = dy.add(dy.double(self.x),(1,0))
		self.y = dy.add(dy.double(self.y),(1,0))

		return -1

	def next(self):

		if(not self.cont):
			self.d1 = self.b.next()
			self.x, self.y = get_digits(self.d1,self.x,self.y)
			self.cont = True

		#print self.x, self.y
		if(self.cont):

			if(dy.leq(self.y,(0,0))):
				#print 'OUT: -1'
				self.cont = True
				return self.emitMinusOne()
			elif(dy.geq(self.x,(-1,1)) and dy.leq(self.y,(1,1))):
				#print 'OUT: 0'
				self.cont = True
				return self.emitZero()
			elif(dy.geq(self.x,(0,0))):
				#print 'OUT: 1'
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

def get_digits(d,minx,maxx):
	#print 'IN:', d
	mid = dy.halve(dy.add(maxx,minx))
	if(d == -1):
		x = minx
		y = mid
	elif(d == 0):
		#print minx, maxx, mid
		x = dy.halve(dy.add(minx,mid))
		y = dy.halve(dy.add(maxx,mid))
	else:
		x = mid
		y = maxx
	return x, y


class multStream(sb.BasicStream):
  def __init__(self,input1,input2):
    self.c = dy.zero
    self.r = 0
    self.inputx = sb.AbsStream(input1) 
    self.inputy = sb.AbsStream(input2)
       # helps if input >= 0, as squaring is then monotone
    self.sqmax = dy.one   # squares of current max/min input values,
    self.sqmin = dy.one   # relative to current output interval
                          # (N.B. square of min may not be min of square!)
    self.xTurn = True

  def processInputDigit(self):
    self.icx = self.inputx.centre()
    self.irx = self.inputx.radius()
    self.icy = self.inputy.centre()
    self.iry = self.inputy.radius()
    # so writing e = 2^-ir, current sqmin and sqmax are respectively
    # ic^2 - 2.ic.e + e^2 and ic^2 + 2.ic.e + e^2, 
    # or rather their residues within the current output interval
    if(self.xTurn):
      self.j = self.inputx.next()
    else:
      self.j = self.inputy.next()


    if(self.xTurn):
      if self.j == sb.one:
        # update sqmin to be icx*icy (by incremental calculation)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icy, -self.irx+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry+self.r))
      elif self.j == sb.minusOne:
        # update sqmax to be icx*icy
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icy, -self.irx+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.irx-self.iry+self.r))
      else: # self.j == zero:
        # update sqmin to be icx*icy + icy*2^(-xr-1) - 2^(-xr-yr-1)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icy, -self.irx-1+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry-1+self.r))
        # update sqmax to be icx*icy - icy*2^(-xr-1) - 2^(-xr-yr-1)
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icy, -self.irx-1+self.r))
        self.sqmax = dy.sub  (self.sqmax, dy.twoTo (-self.irx-self.iry-1+self.r))
      self.xTurn = False

    else:
      if self.j == sb.one:
        # update sqmin to be icx*icy (by incremental calculation)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icx, -self.iry+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.iry-self.irx+self.r))
      elif self.j == sb.minusOne:
        # update sqmax to be icx*icy
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icx, -self.iry+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.iry-self.irx+self.r))
      else: # self.j == zero:
        # update sqmin to be icx*icy + icx*2^(-yr-1) - 2^(-xr-yr-1)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icx, -self.iry-1+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry-1+self.r))
        # update sqmax to be icx*icy - icx*2^(-yr-1) - 2^(-xr-yr-1)
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icx, -self.iry-1+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.irx-self.iry-1+self.r))
      self.xTurn = True

  def emitOne(self):
    self.sqmin = sb.double (sb.add (self.sqmin, dy.minusHalf))
    self.sqmax = sb.double (sb.add (self.sqmax, dy.minusHalf))
    return sb.BasicStream.emitOne (self)
  def emitZero(self):
    self.sqmin = sb.double (self.sqmin)
    self.sqmax = sb.double (self.sqmax)
    return sb.BasicStream.emitZero (self)
  def emitMinusOne(self):
    self.sqmin = sb.double (sb.add (self.sqmin, dy.half))
    self.sqmax = sb.double (sb.add (self.sqmax, dy.half))
    return sb.BasicStream.emitMinusOne (self)

  def next(self):
    # Case 1: input interval straddles 0, so min square is 0 rather than sqmin
    
    if(self.xTurn and sb.lt (self.inputx.min(), dy.zero)):
        if self.r == 0:
          return self.emitOne()  # first digit 1 comes for free
        # otherwise, 0 is lower limit of output range, so -1 the only possibility
        elif sb.leq (self.sqmax, dy.zero):
          return self.emitMinusOne()
        else: # need more input info before we can emit
          self.processInputDigit()
          return self.next()
    elif sb.lt (self.inputy.min(), dy.zero):
        if self.r == 0:
          return self.emitOne()  # first digit 1 comes for free
        # otherwise, 0 is lower limit of output range, so -1 the only possibility
        elif sb.leq (self.sqmax, dy.zero):
          return self.emitMinusOne()
        else: # need more input info before we can emit
          self.processInputDigit()
          return self.next()

    # Case 2: sqmin is min of square
    elif sb.leq (dy.zero, self.sqmin): 
      return self.emitOne()
    elif sb.leq (self.sqmax, dy.zero): 
      return self.emitMinusOne()
    elif sb.leq (dy.minusHalf, self.sqmin) and sb.leq (self.sqmax, dy.half): 
      return self.emitZero()
    else: # need more input info before we can emit
      self.processInputDigit()
      return self.next() # recursion: will process as many input digits as necessary

#turns a decimal stream into a binary stream
class DStoBS(sb.BasicStream):
	def __init__(self,decimal):
		sb.BasicStream.__init__(self)
		self.d = decimal
		self.d1 = (0,0)
		self.x = (0,0)
		self.y = (1,0)
		self.cont = False
		self.end = False
		self.isFirst = True

	#before each emit it resizes the interval [minx,maxx] depending
	#on the digit being emitted (-1,0 or 1)
	def emitOne(self):
		self.x = dec.multD(dec.subD(self.x,(5,-1)),(2,0))
		self.y = dec.multD(dec.subD(self.y,(5,-1)),(2,0))
		return 1

	def emitZero(self):
		self.x = dec.multD(dec.subD(self.x,(25,-2)),(2,0))
		self.y = dec.multD(dec.subD(self.y,(25,-2)),(2,0))
		return 0

	def emitMinusOne (self):
		self.x = dec.multD(self.x,(2,0))
		self.y = dec.multD(self.y,(2,0))
		return -1

	#emits next digit
	def next(self):
		#if we do not have enough information to emit next digit
		#we request another input digit from our input stream
		if(self.isFirst):
			self.isFirst = False
			return 1

		if(not self.cont):
			self.d1 = dec.fromIntD(self.d.next())
			self.x, self.y = get_digitsD(self.d1,self.x,self.y)
			self.cont=True

		#if we do have enough info we check what interval it fits
		#in and emit the digit corresponding to it
		if(self.cont):
			if(dec.leqD(self.y,(5,-1))):
				self.cont = True
				return self.emitMinusOne()
			elif(dec.geqD(self.x,(25,-2)) and dec.leqD(self.y,(75,-2))):
				self.cont = True
				return self.emitZero()
			elif(dec.geqD(self.x,(5,-1))):
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

def get_digitsD(d,minx,maxx):
	x = dec.addD(dec.multD(dec.multD(d,(dec.subD(maxx,minx))),(1,-1)), minx)
	y = dec.addD(dec.multD(dec.multD(dec.addD(d,(1,0)),dec.subD(maxx,minx)),(1,-1)), minx)
	#x = (d)*(maxx-minx)*.1 +minx
	#y = (d+1)*(maxx-minx)*.1 +minx
	return x, y

class BStoDS(dec.DecimalStream):
	def __init__(self,binS):
		self.b = binS[0]
		self.exp = 2**binS[1]
		self.log = int(m.ceil(m.log(self.exp,10)))
		self.d1 = (0,0)
		self.x = (-self.exp,-self.log)
		self.y = (self.exp,-self.log)
		self.cont = False

	def emit(self, a):
		#x = x*10 - a
		self.x = dec.subD(dec.multD(self.x,(1,1)),(a,0))
		#y = y*10 - a
		self.y = dec.subD(dec.multD(self.y,(1,1)),(a,0))
		return a

	def next(self):
		#print self.log

		if(not self.cont):
			#print "HERE 1"
			self.d1 = self.b.next()
			self.x, self.y = get_digitsB(self.d1,self.x,self.y)
			self.cont = True

		#print "HERE 3"
		#print self.x, self.y
		#could use loop
		if(self.cont):
			if(dec.geqD(self.x,(-1,-1)) and dec.leqD(self.y,(1,-1))):
				self.cont = True
				return self.emit(0)

			elif(dec.leqD(self.y,(-8,-1))):
				self.cont = True
				return self.emit(-9)

			elif(dec.geqD(self.x,(-9,-1)) and dec.leqD(self.y,(-7,-1))):
				self.cont = True
				return self.emit(-8)

			elif(dec.geqD(self.x,(-8,-1)) and dec.leqD(self.y,(-6,-1))):
				self.cont = True
				return self.emit(-7)

			elif(dec.geqD(self.x,(-7,-1)) and dec.leqD(self.y,(-5,-1))):
				self.cont = True
				return self.emit(-6)

			elif(dec.geqD(self.x,(-6,-1)) and dec.leqD(self.y,(-4,-1))):
				self.cont = True
				return self.emit(-5)

			elif(dec.geqD(self.x,(-5,-1)) and dec.leqD(self.y,(-3,-1))):
				self.cont = True
				return self.emit(-4)

			elif(dec.geqD(self.x,(-4,-1)) and dec.leqD(self.y,(-2,-1))):
				self.cont = True
				return self.emit(-3)

			elif(dec.geqD(self.x,(-3,-1)) and dec.leqD(self.y,(-1,-1))):
				self.cont = True
				return self.emit(-2)

			elif(dec.geqD(self.x,(-2,-1)) and dec.leqD(self.y,(0,0))):
				self.cont = True
				return self.emit(-1)

			elif(dec.geqD(self.x,(0,0)) and dec.leqD(self.y,(2,-1))):
				self.cont = True
				return self.emit(1)

			elif(dec.geqD(self.x,(1,-1)) and dec.leqD(self.y,(3,-1))):
				self.cont = True
				return self.emit(2)

			elif(dec.geqD(self.x,(2,-1)) and dec.leqD(self.y,(4,-1))):
				self.cont = True
				return self.emit(3)

			elif(dec.geqD(self.x,(3,-1)) and dec.leqD(self.y,(5,-1))):
				self.cont = True
				return self.emit(4)

			elif(dec.geqD(self.x,(4,-1)) and dec.leqD(self.y,(6,-1))):
				self.cont = True
				return self.emit(5)

			elif(dec.geqD(self.x,(5,-1)) and dec.leqD(self.y,(7,-1))):
				self.cont = True
				return self.emit(6)

			elif(dec.geqD(self.x,(6,-1)) and dec.leqD(self.y,(8,-1))):
				self.cont = True
				return self.emit(7)

			elif(dec.geqD(self.x,(7,-1)) and dec.leqD(self.y,(9,-1))):
				self.cont = True
				return self.emit(8)

			elif(dec.geqD(self.x,(8,-1))):
				self.cont = True
				return self.emit(9)

			#if the interval does not fit into these intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()

def get_digitsB(d,minx,maxx):
	#print 'IN:', d, minx, maxx
	mid = dec.multD(dec.addD(maxx,minx),(5,-1))
	#print mid
	if(d == -1):
		x = minx
		y = mid
	elif(d == 0):
		#print minx, maxx, mid
		x = dec.multD(dec.addD(minx,mid),(5,-1))
		y = dec.multD(dec.addD(maxx,mid),(5,-1))
	else:
		x = mid
		y = maxx
	#print "HERE 2"
	return x, y

#exp won't be right until next is called at least once
class inverse(dec.DecimalStream):
	def __init__(self,stream):
		self.stream = sb.AbsStream(stream[0])
		self.exp = stream[1]
		self.ctr = (0,0)
		self.r = 0
		self.min = (0,0)
		self.max = (0,0)
		self.imin = (0,0)
		self.imax = (0,0)
		self.omin = (-1,1)
		self.omax = (1,1)
		self.cont = True

	def readDig(self):
		new = self.stream.next()
		self.r += 1

		self.ctr = dy.add(self.ctr,(new,self.r))
		self.min = dy.sub(self.ctr, (1,self.r))
		self.max = dy.add(self.ctr, (1,self.r))

		self.imin = rat.inv(rat.fromDy(self.max))
		self.imax = rat.inv(rat.fromDy(self.min))
		#print self.imin, self.imax

	def emitMinusOne(self):
		#print "minusOne"
		self.omax = rat.mult(rat.add(self.omin, self.omax),(1,2))
		return -1

	def emitZero(self):
		#print "zero"
		mid = rat.mult(rat.add(self.omin, self.omax),(1,2))

		self.omin = rat.mult(rat.add(self.omin, mid),(1,2))
		self.omax = rat.mult(rat.add(self.omax, mid),(1,2))
		return 0

	def emitOne(self):
		#print "one"
		self.omin = rat.mult(rat.add(self.omin, self.omax),(1,2))
		return 1

	def initialise(self):
		dig = self.stream.next()
		self.exp -= 1
			
		while(dig == 0):
			dig = self.stream.next()
			self.exp -= 1
		dig2 = self.stream.next() 
		self.exp -= 1
		tot = dig*2 +dig2

		while(dig2 == -1):
			dig2 = self.stream.next()
			self.exp -= 1
			tot = tot*2+dig2

		self.ctr = (tot, 0)
		self.min = dy.sub(self.ctr, (1,self.r))
		self.max = dy.add(self.ctr, (1,self.r))
		self.imin = rat.inv(rat.fromDy(self.max))
		self.imax = rat.inv(rat.fromDy(self.min))

		self.start = False

	def next(self):
		if(not self.cont):
			#print "read"
			self.readDig()
			self.cont = True
			return self.next()

		else:
			mid = rat.mult(rat.add(self.omin, self.omax),(1,2))
			q1 = rat.mult(rat.add(self.omin, mid),(1,2))
			q3 = rat.mult(rat.add(self.omax, mid),(1,2))

			#print self.imin, self.imax
			#print self.omin, q1, mid, q3, self.omax

			if(rat.geq(self.imin, self.omin) and rat.leq(self.imax, mid)):
				self.cont = True
				return self.emitMinusOne()

			elif(rat.geq(self.imin, q1) and rat.leq(self.imax, q3)):
				self.cont = True
				return self.emitZero()

			elif(rat.geq(self.imin, mid) and rat.leq(self.imax, self.omax)):
				self.cont = True
				return self.emitOne()
			#if the interval does not fit into these three intervals
			#it means we do not have enough info so we recurse
			else:
				self.cont = False
				return self.next()







		




