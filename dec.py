#modified version of dy.py for establishing the decimal representation
#(a,b) = a*10^b

#abstract class similar to BasicStream in sb.py but for decimals
class DecimalStream:
	def __init__(self):
		self.r = 0

	def radius(self):
		return self.r

	def emit(self, dig):
		self.r +=1
		return dig

	def next(self):
		return self.emit()

	def nextn (self, n):
		return [self.next() for i in range(n)]

#simplifies the decimal digit representation to it's smallest integers
def simpD(p,k):

	if(p%10 == 0 and p != 0):
		return simpD(p/10,k+1)
	else:
		return (p,k)

#negates the number
def negD(d):
	return (-d[0], d[1])

#adds 2 numbers
def addD(d,e):
	if d[1] >= e[1]: 
		return simpD(e[0] + (d[0]*10**(d[1]-e[1])), e[1])
	else:
		return simpD(d[0] + (e[0]*10**(e[1]-d[1])), d[1])

#subtracts 2 numbers
def subD(d,e):
	return addD(d, negD(e))

#halves a number
def halveD(d):
	return simpD(d[0]/2, d[1])

#multiplies 2 numbers
def multD(d,e):
	return simpD(d[0]*e[0], d[1]+e[1])

#checks whether the first number is smaller than the second
def ltD(d,e):
	if(d[1] >= e[1]):
		return e[0] > (d[0]*10**(d[1]-e[1]))
	else:
		return (e[0]*10**(e[1]-d[1])) > d[0]
#checks whether the first number is smaller or equal to the second
def leqD(d,e):
	return not ltD(e,d)
#checks whether the first number is greater or equal to the second
def geqD(d,e):
	return not ltD(d,e)
#turns a digit into the digit representation (eg. 40 -> (4,1))
def fromIntD(n):
	return simpD(n,0)
#turns tuple to digit
def toIntD(d):
	return d[0]*10**d[1]