import external.sb as sb

class BS_Factory(sb.BasicStream):
	def __init__(self,inputS):
		sb.BasicStream.__init__(self)
		self.origin = inputS
		self.dlist = DList()		
		self.n = 0

	def copy(self):
		newS = NBS(self.origin, self.dlist)
		return newS

class NBS(sb.BasicStream):
	def __init__(self,origin, dlist):
		sb.BasicStream.__init__(self)
		self.origin = origin
		self.dlist = dlist
		self.p = 0
	def next(self):
		if self.p < self.dlist.len():
			d = self.dlist.atP(self.p)
			self.p+=1
			return d
		else:
			d = self.origin.next()
			self.dlist.addD(d)
			self.p+=1
			return d

class DList:
	def __init__(self):
		self.dlist = []

	def addD(self,d):
		self.dlist.append(d)

	def returnDList(self):
		return self.dlist

	def len(self):
		return len(self.dlist)

	def atP(self,p):
		return self.dlist[p]



