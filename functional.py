import BS_Factory as fact
import external.average as av
import helper as h
import OOP as o
import math as m
import external.sb as sb

def multByInt(binS, intg):
	binInt = h.digToB1(intg)
	binSF = fact.BS_Factory(binS)
	mult = []
	i = 0
	while(i<len(binInt)):
		if(binInt[i] == '1'):
			mult.append(i+1)
		i+=1

	if(len(mult)>1):
		#result = f.divByDig(binSF.copy(),2**mult[len(mult)-1])
		result = h.shiftRightBy(binSF.copy(),mult[len(mult)-1])
		last = mult[0]
		mult = mult[1:-1]
		for j in mult:
			result = h.add(result,h.shiftRightBy(binSF.copy(),j))
			#result = add(result,f.divByDig(binSF.copy(),2**j))
		result = av.AverageStream(result, h.shiftRightBy(binSF.copy(),last))
		#result = av.AverageStream(result, f.divByDig(binSF.copy(),2**last))
	else:
		result = h.shiftRightBy(binSF.copy(), mult[0])
		#result = f.divByDig(binSF.copy(),2**mult[0])

	return result

#1/2^7
class delayedDStoBS(sb.BasicStream):
	def __init__(self,rest):
		sb.BasicStream.__init__(self)
		self.started = False
		self.result = 0
		self.rest = fact.BS_Factory(rest)

	def next(self):
		if(not self.started):
			self.result = DStoBS_f(self.rest.copy())
			self.started = True
			return self.result.next()
		else:
			return self.result.next()

def DStoBS_f(decS):
	a = decS.next()
	b = decS.next()
	c = h.digToBS(10*a+b)
	return o.divByInt1(av.AverageStream(c,delayedDStoBS(decS)),50)