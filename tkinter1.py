# -*- coding: utf-8 -*-

from Tkinter import *
import ttk
import tkFont
from PIL import Image, ImageTk
import toR as r
import external.sb as sb
import finalWrapper as fw

def scroll(entry, *L):
	global max_scroll
	op, howMany = L[0], L[1]
        if op == 'scroll':
            units = L[2]
            entry.xview_scroll(howMany, units)
        elif op == 'moveto':
        	entry.xview_moveto(howMany)
        	if(entry.xview()[1]>=0.9):
        		value = final_result.nextn(10)
        		meters.set(value)

def function_1arg(var):
	global funcInput1, funcInput2, resObjs, outputs

	x = funcInput1

	if x is not None:
		create_output()
		# shift resObjs and outputs by 1
		for i in range(len(resObjs)-1, 0, -1):
			resObjs[i] = resObjs[i-1]
			outputs[i].set(outputs[i-1].get())

		string, op = functions_2args[var]
		if(var == "e"):
			info = string + xinput.get() + " "
		else:
			info = " " +  xinput.get() + string 
		outputs[0].set(info)
		ops.set(outputs[0].get())
		resObjs[0] = op(x)

		funcInput1 = None
		funcInput2 = None
		xinput.set("")
		yinput.set("")

	input_1.focus()


def function_2args(var):
	global funcInput1, funcInput2, resObjs, outputs, int_y

	x = funcInput1
	if(var == "divI" or var == "multI"):
		y = int_y
	else:
		y = funcInput2

	if x is not None and y is not None:
		create_output()
		# shift resObjs and outputs by 1
		for i in range(len(resObjs)-1, 0, -1):
			resObjs[i] = resObjs[i-1]
			outputs[i].set(outputs[i-1].get())

		string, op = functions_2args[var]
		info = "(" + xinput.get() + string.decode('utf-8') + yinput.get() + ")"
		outputs[0].set(info)
		ops.set(outputs[0].get())
		resObjs[0] = op(x,y)

		# clear everything
		funcInput1 = None
		funcInput2 = None
		int_y = None
		xinput.set("")
		yinput.set("")

	input_1.focus()

def setOutput(var, index):
	global funcInput1, funcInput2, resObjs, outputs

	if var == 'x':
		funcInput1 = resObjs[index]
		xinput.set(outputs[index].get())
	else:
		funcInput2 = resObjs[index]
		yinput.set(outputs[index].get())
	outputs[index].set("")
	resObjs[index] = None

	delete_output(index)
	input_1.focus()


def setInput(var):
	global funcInput1, funcInput2, int_y

	# try/except in case called with no input, to refocus on input 
	try:
		if var == 'x':
			funcInput1 = fw.inputFloat(float(input1.get()))
			xinput.set(input1.get())
		else:
			funcInput2 = fw.inputFloat(float(input1.get()))
			int_y = int(float(input1.get()))
			yinput.set(input1.get())
	except:
		pass

	input1.set("")
	input_1.focus()

def create_output():
	global num_res, resObjs, outputs, buttons
	tick = '✓'
	num_res += 1
	out_id = num_res-1
	outputs.append(StringVar())
	resObjs.append(None)

	#entry
	new_out = ttk.Entry(mainframe, state=DISABLED, width=15, textvariable=outputs[out_id])
	row_num = 5+num_res
	new_out.grid(column=5, row=row_num, columnspan=2, sticky=(W, E))


	#buttons
	new_butx = ttk.Button(mainframe, width=1, text=tick, command=lambda: setOutput('x',out_id))
	new_butx.grid(column=7, row=row_num, sticky=W)
	new_buty = ttk.Button(mainframe, width=1, text=tick, command=lambda: setOutput('y',out_id))
	new_buty.grid(column=8, row=row_num, sticky=W)
	buttons.append((new_out, new_butx, new_buty))

def delete_output(index):
	global num_res, buttons, resObjs, outputs
	num_res -= 1
	for b in buttons[index]:
		b.destroy()

	buttons.pop(index)
	resObjs.pop(index)
	outputs.pop(index)

	for i in range(len(buttons)):
		button = buttons[i]
		button[1].configure(command=lambda: setOutput('x',i))
		button[2].configure(command=lambda: setOutput('y',i))

		for b in button:
			new_col = 6+i
			b.grid(row=new_col)


def calculate():
	global funcInput1, funcInput2, resObjs, outputs, final_result, num_res

	final_result = fw.result(resObjs[0])
	value = final_result.nextn(70)
	meters.set(value)
	ops.set(outputs[0].get())
	funcInput1 = None
	funcInput2 = None
	xinput.set("")
	yinput.set("")
	input1.set("")

	# delete all buttons when equals is pressed
	while buttons:
		delete_output(0)

	input_1.focus()

    
root = Tk()

style = ttk.Style()
style.theme_use('aqua')
root.title("Exact Real Number Calculator")
helv36 = tkFont.Font(family='Helvetica', size=36, weight=tkFont.BOLD)

mainframe = ttk.Frame(root, padding="12 12 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

#scrolling data
max_scroll = 0

#output registers
num_res = 0
buttons = []
final_result = None
resObjs = []
outputs = []

#FUNCTIONS
functions_2args = {"add": ('+', r.addR),
				"sub": ('-', r.subR),
				"mult": ('*', r.multR),
				"div": ('/', r.divR),
				"avg": ('⊕', r.averageR),
				"multI": ('*', r.multByIntR),
				"divI": ('/', r.divByIntR),
				"sq": ('^2', r.squareR),
				"e": ('e^', r.expR)}

ops = StringVar()
ops.set("Operation")
int_y = None
funcInput1 = None
funcInput2 = None
xinput = StringVar()
yinput = StringVar()
input1 = StringVar()
input2 = StringVar()
meters = StringVar()
meters.set("ULTIMATE CALCULATOR/FAX MACHINE")

#output box with scrollbar
output_box = ttk.Entry(mainframe, font = "Courier 25", state=DISABLED, width=20, textvariable=meters)
s = Scrollbar(mainframe, orient=HORIZONTAL)
s.config(command=lambda *L: scroll(output_box, *L))
s.grid(column=1, row=3, columnspan=9, sticky=(W,E))
output_box.grid(column=1, row=2, columnspan=9, sticky=(W, E))
output_box.configure(xscrollcommand=s.set)

#resulting operation
resop = ttk.Entry(mainframe, font = "Courier 25", state=DISABLED, width=20, textvariable=ops)
resop.grid(column=1, row=1, columnspan=7, sticky=(W, E))

#input 1
input_1 = ttk.Entry(mainframe, width=15, textvariable=input1)
input_1.grid(column=5, row=5, columnspan=2, sticky=(W, E))

#function buttons
ttk.Button(mainframe, width=1, text="=", command=calculate).grid(column=8, row=1, sticky=(W, E))
ttk.Button(mainframe, width=5, text="x/int", command=lambda: function_2args("divI")).grid(column=1, row=5, sticky=W)
ttk.Button(mainframe, width=5, text="x*int", command=lambda: function_2args("multI")).grid(column=2, row=5, sticky=W)
ttk.Button(mainframe, width=5, text="x*y", command=lambda: function_2args("mult")).grid(column=3, row=5, sticky=W)
ttk.Button(mainframe, width=5, text="x/y", command=lambda: function_2args("div")).grid(column=1, row=6, sticky=W)
ttk.Button(mainframe, width=5, text="x+y", command=lambda: function_2args("add")).grid(column=2, row=6, sticky=W)
ttk.Button(mainframe, width=5, text="x-y", command=lambda: function_2args("sub")).grid(column=3, row=6, sticky=W)
ttk.Button(mainframe, width=5, text="x⊕y", command=lambda: function_2args("avg")).grid(column=1, row=7, sticky=W)
ttk.Button(mainframe, width=5, text="x^2", command=lambda: function_1arg("sq")).grid(column=2, row=7, sticky=W)
ttk.Button(mainframe, width=5, text="e^x", command=lambda: function_1arg("e")).grid(column=3, row=7, sticky=W)

#input x y buttons
tick = '✓'
ttk.Button(mainframe, width=1, text=tick, command=lambda: setInput('x')).grid(column=7, row=5)
ttk.Button(mainframe, width=1, text=tick, command=lambda: setInput('y')).grid(column=8, row=5)

ttk.Label(mainframe, font=("Courier", 15), text="INPUT:").grid(column=4, row=5, sticky=E)
ttk.Label(mainframe, font=("Courier", 15), text="OUTPUT:").grid(column=4, row=6, sticky=E)
ttk.Label(mainframe, font=("Courier", 15), text="x").grid(column=7, row=4)
ttk.Label(mainframe, font=("Courier", 15), text="y").grid(column=8, row=4)


#x
x_view = ttk.Entry(mainframe, state=DISABLED, width=5, textvariable=xinput)
x_view.grid(column=1, columnspan=2, row=10, sticky=(W,E))
#y
y_view = ttk.Entry(mainframe, state=DISABLED, width=5, textvariable=yinput)
y_view.grid(column=3, columnspan=2, row=10, sticky=(W,E))

ttk.Label(mainframe, font=("Courier", 15), text="x").grid(column=1, columnspan=2, row=9)
ttk.Label(mainframe, font=("Courier", 15), text="y").grid(column=3, columnspan=2, row=9)

for child in mainframe.winfo_children(): child.grid_configure(padx=3, pady=3)

input_1.focus()
root.bind('<Return>', calculate)

root.mainloop()