import external.dy as dy
import external.sb as sb
import math as m

class multStream(sb.BasicStream):
  def __init__(self,input1,input2):
    self.c = dy.zero
    self.r = 0
    self.inputx = sb.AbsStream(input1) 
    self.inputy = sb.AbsStream(input2)
       # helps if input >= 0, as squaring is then monotone
    self.sqmax = dy.one   # squares of current max/min input values,
    self.sqmin = dy.one   # relative to current output interval
                          # (N.B. square of min may not be min of square!)
    self.xTurn = True
    self.start = True

  def processInputDigit(self):
    self.icx = self.inputx.centre()
    self.irx = self.inputx.radius()
    self.icy = self.inputy.centre()
    self.iry = self.inputy.radius()
    # so writing e = 2^-ir, current sqmin and sqmax are respectively
    # ic^2 - 2.ic.e + e^2 and ic^2 + 2.ic.e + e^2, 
    # or rather their residues within the current output interval
    if(self.xTurn):
      self.j = self.inputx.next()
    else:
      self.j = self.inputy.next()


    if(self.xTurn):
      if self.j == sb.one:
        # update sqmin to be icx*icy (by incremental calculation)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icy, -self.irx+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry+self.r))
        self.xTurn = False
        self.start = False
      elif self.j == sb.minusOne:
        # update sqmax to be icx*icy
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icy, -self.irx+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.irx-self.iry+self.r))
        self.xTurn = False
        self.start = False
      else: # self.j == zero:
        # update sqmin to be icx*icy + icy*2^(-xr-1) - 2^(-xr-yr-1)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icy, -self.irx-1+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry-1+self.r))
        # update sqmax to be icx*icy - icy*2^(-xr-1) - 2^(-xr-yr-1)
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icy, -self.irx-1+self.r))
        self.sqmax = dy.sub  (self.sqmax, dy.twoTo (-self.irx-self.iry-1+self.r))
      	if(not self.start):
      		self.xTurn = False

    else:
      if self.j == sb.one:
        # update sqmin to be icx*icy (by incremental calculation)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icx, -self.iry+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.iry-self.irx+self.r))
      elif self.j == sb.minusOne:
        # update sqmax to be icx*icy
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icx, -self.iry+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.iry-self.irx+self.r))
      else: # self.j == zero:
        # update sqmin to be icx*icy + icx*2^(-yr-1) - 2^(-xr-yr-1)
        self.sqmin = dy.add (self.sqmin, dy.scale (self.icx, -self.iry-1+self.r))
        self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.irx-self.iry-1+self.r))
        # update sqmax to be icx*icy - icx*2^(-yr-1) - 2^(-xr-yr-1)
        self.sqmax = dy.sub (self.sqmax, dy.scale (self.icx, -self.iry-1+self.r))
        self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.irx-self.iry-1+self.r))
      self.xTurn = True

  def emitOne(self):
    self.sqmin = sb.double (sb.add (self.sqmin, dy.minusHalf))
    self.sqmax = sb.double (sb.add (self.sqmax, dy.minusHalf))
    return sb.BasicStream.emitOne (self)
  def emitZero(self):
    self.sqmin = sb.double (self.sqmin)
    self.sqmax = sb.double (self.sqmax)
    return sb.BasicStream.emitZero (self)
  def emitMinusOne(self):
    self.sqmin = sb.double (sb.add (self.sqmin, dy.half))
    self.sqmax = sb.double (sb.add (self.sqmax, dy.half))
    return sb.BasicStream.emitMinusOne (self)

  def next(self):
    # Case 1: input interval straddles 0, so min square is 0 rather than sqmin
    
    if(self.xTurn and sb.lt (self.inputx.min(), dy.zero)):
        if self.r == 0:
          return self.emitOne()  # first digit 1 comes for free
        # otherwise, 0 is lower limit of output range, so -1 the only possibility
        elif sb.leq (self.sqmax, dy.zero):
          return self.emitMinusOne()
        else: # need more input info before we can emit
          self.processInputDigit()
          return self.next()
    elif sb.lt (self.inputy.min(), dy.zero):
        if self.r == 0:
          return self.emitOne()  # first digit 1 comes for free
        # otherwise, 0 is lower limit of output range, so -1 the only possibility
        elif sb.leq (self.sqmax, dy.zero):
          return self.emitMinusOne()
        else: # need more input info before we can emit
          self.processInputDigit()
          return self.next()
          
    # Case 2: sqmin is min of square
    elif sb.leq (dy.zero, self.sqmin): 
      return self.emitOne()
    elif sb.leq (self.sqmax, dy.zero): 
      return self.emitMinusOne()
    elif sb.leq (dy.minusHalf, self.sqmin) and sb.leq (self.sqmax, dy.half): 
      return self.emitZero()
    else: # need more input info before we can emit
      self.processInputDigit()
      return self.next() # recursion: will process as many input digits as necessary