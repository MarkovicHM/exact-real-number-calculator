from fractions import gcd

def simp(x):
	a = gcd(x[0],x[1])
	if(not a == 1):
		return (x[0]/a,x[1]/a)
	else:
		return x

def neg(x):
	return (-x[0],x[1])

def add(x,y):
	return simp((x[0]*y[1]+y[0]*x[1],y[1]*x[1]))

def sub(x,y):
	return simp(add(x,neg(y)))

def mult(x,y):
	return simp((x[0]*y[0],x[1]*y[1]))

def lt(x,y):
	return x[0]*y[1] < y[0]*x[1]

def gt(x,y):
	return x[0]*y[1] > y[0]*x[1]

def leq(x,y):
	return x[0]*y[1] <= y[0]*x[1]

def geq(x,y):
	return x[0]*y[1] >= y[0]*x[1]

def inv(x):
	return (x[1],x[0])

def fromDy(x):
	return (x[0],2**x[1])
#1/3>3/4
#4/12>9/12