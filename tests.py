import timeit
import progressbar as pro

setup = '''
import functional as f
import OOP as o
import helper as h
import BS_Factory as fact

a = h.ExpDecS("011")
b = fact.BS_Factory(a)
'''

def testM_f(n, loops=1000):
	s = setup + 'n={}'.format(n)
	return timeit.timeit('f.multByInt(b.copy(),6).nextn(n)',setup=s, number=loops)/loops

def testM_o(n, loops=1000):
	s = setup + 'n={}'.format(n)
	return timeit.timeit('o.multByInt(b.copy(),6).nextn(n)',setup=s, number=loops)/loops

def testD_f(n, loops=1000):
	s = setup + 'n={}'.format(n)
	return timeit.timeit('o.divByInt2(b.copy(),13).nextn(n)',setup=s, number=loops)/loops

def testD_o(n, loops=1000):
	s = setup + 'n={}'.format(n)
	return timeit.timeit('o.divByInt1(b.copy(),13).nextn(n)',setup=s, number=loops)/loops

setup2 = '''
import functional as f
import helper as h
import BS_Factory as fact

a = h.ExpDecS("314")
b = fact.BS_Factory(a)
'''

def testDtB_f(n, loops=100):
	s = setup2 + 'n={}'.format(n)
	return timeit.timeit('f.DStoBS_f(b.copy()).nextn(n)',setup=s,number=loops)/loops

def testDtB_o(n, loops=1000):
	s = setup2 + 'n={}'.format(n)
	return timeit.timeit('o.DStoBS(b.copy()).nextn(n)',setup=s,number=loops)/loops

def testAll():
	results = []
	for func in (testM_f, testM_o, testD_f, testD_o, testDtB_f,testDtB_o):
		p = pro.ProgressBar(widgets=[pro.Percentage(),' ', pro.ETA(), ' ',pro.Bar()])
		results.append([func(n) for n in p(xrange(10,1100,100))])

	return results