import dy
from dy import *
import sb
from sb import *
import my

class AverageStream(BasicStream):
    def __init__(self,inputx,inputy):
        self.inputx=inputx
        self.inputy=inputy
        self.c = dy.zero  # centre of current range
        self.r = 0  # - log2 (radius of current range)
        self.turn=0
        self.state=0

    def next(self):
        while True:
            """state=0"""
            if(self.turn==0 and self.state==0) :
                # print("*****   state 0   ******")
                self.i=self.inputx.next()
                # print("Input x:",self.i)
                if(self.i==-1):
                    self.state=1
                    self.turn=1

                if(self.i==0):
                    self.state=2
                    self.turn=1

                if(self.i==1):
                    self.state=3
                    self.turn=1
            """state=1"""
            if(self.turn==1 and self.state==1):
                # print("*****  state 1  ******")
                self.i = self.inputy.next()
                # print("Input y:", self.i)

                """state=4, go back to [-1,1]"""
                if(self.i==-1):
                    # print("*****  state 4  ******")
                    self.state=0
                    self.turn=0
                    return self.emitMinusOne()
                """state=5, input the second x"""
                if(self.i==0):
                    # print("*****  state 5  ******")
                    self.state=5
                    self.turn=0

                """state=6, go back to [-1,1]"""
                if(self.i==1):
                    # print("*****  state 6  ******")
                    self.state=0
                    self.turn=0
                    return self.emitZero()

            """state=2"""
            if(self.turn==1 and self.state==2):
                # print("*****  state 2  ******")
                self.i=self.inputy.next()
                # print("Input y1:", self.i)

                """state=7, ask to input another x"""
                if(self.i==-1):
                    # print("*****  state 7  ******")
                    self.state=7
                    self.turn=0

                """state=8, go back to [-1,1]"""
                if(self.i==0):
                    # print("*****  state 8  ******")
                    self.state=0
                    self.turn=0
                    return self.emitZero()

                """state=9, ask to input anther x"""
                if(self.i==1):
                    # print("*****  state 9  ******")
                    self.state=9
                    self.turn=0

            """state=3"""
            if(self.turn==1 and self.state==3):
                # print("*****  state 3  ******")
                self.i=self.inputy.next()
                # print("Input y1:", self.i)
                """state=10, go back to [-1,1]"""
                if(self.i==-1):
                    # print("*****  state 10  ******")
                    self.state=0
                    self.turn=0
                    # print("state:",self.state)
                    # print("turn:",self.turn)
                    return self.emitZero()

                """state=11, ask to input another x"""
                if(self.i==0):
                    # print("*****  state 11  ******")
                    self.state=11
                    self.turn=0

                """state=12, go back to [-1,1]"""
                if(self.i==1):
                    # print("*****  state 12  ******")
                    self.state=0
                    self.turn=0
                    # print("state",self.state)
                    # print("turn", self.turn)
                    return self.emitOne()

            """the branches when state=5"""

            if(self.turn==0 and self.state==5):
                # print("*****  state 5 branch  ******")
                self.i = self.inputx.next()
                # print("Input x:",self.i)
                if(self.i)==-1:
                    # print("*****  state 5(-1) branch  ******")
                    self.state=3
                    self.turn=1
                    # print("state:",self.state)
                    # print("turn:",self.turn)
                    return self.emitMinusOne()

                if(self.i==0):
                    # print("*****  state 5(0) branch  ******")
                    self.turn=0
                    self.i=self.inputy.next()
                    # print("Input y",self.i)
                    if(self.i==-1):
                        # print("*****  state 5(0-1) branch  ******")
                        self.state=11
                        return self.emitMinusOne()
                    if(self.i==0):
                        # print("*****  state 5(0-2) branch  ******")
                        self.state=12
                        return self.emitMinusOne()
                    if(self.i==1):
                        # print("*****  state 5(0-3) branch  ******")
                        self.state=5
                        return self.emitZero()


                if(self.i==1):
                    # print("*****  state 5(1) branch  ******")
                    self.state=1
                    self.turn=1
                    return self.emitZero()

            """the branches when state=7"""

            if (self.turn == 0 and self.state == 7):
                # print("*****  state 7 branch  ******")
                self.i = self.inputx.next()
                # print("Input x:", self.i)
                if (self.i) == -1:
                    # print("*****  state 7(-1) branch  ******")
                    self.state = 3
                    self.turn = 1
                    return self.emitMinusOne()

                if (self.i == 0):
                    # print("*****  state 7(0) branch  ******")
                    self.i = self.inputy.next()
                    # print("Input y2", self.i)
                    if (self.i == -1):
                        # print("*****  state 7(0-1) branch  ******")
                        self.state = 11
                        return self.emitMinusOne()
                    if (self.i==0):
                        # print("*****  state 7(0-2) branch  ******")
                        self.state=12
                        # print(self.turn)
                        # self.turn=0
                        return self.emitMinusOne()
                    if (self.i==1):
                        # print("*****  state 7(0-3) branch  ******")
                        self.state=5
                        return self.emitZero()
                if(self.i==1):
                    # print("*****  state 7(1) branch  ******")
                    self.state=1
                    self.turn=1
                    return self.emitZero()

            """the branches when state=9"""

            if (self.turn == 0 and self.state == 9):
                # print("*****  state 9 branch  ******")
                self.i = self.inputx.next()
                # print("Input x2:", self.i)
                if (self.i) == -1:
                    # print("*****  state 9(-1) branch  ******")
                    self.state = 3
                    self.turn = 1
                    return self.emitZero()

                if (self.i == 0):
                    # print("*****  state 9-0 branch  ******")
                    self.i = self.inputy.next()
                    # print("Input y2", self.i)
                    if (self.i == -1):
                        # print("*****  state 9(0-1) branch  ******")
                        self.state = 11
                        return self.emitZero()
                    if (self.i == 0):
                        # print("*****  state 9(0-2) branch  ******")
                        self.state = 4
                        return self.emitOne()
                    if (self.i == 1):
                        # print("*****  state 9(0-3) branch  ******")
                        self.state = 5
                        return self.emitOne()
                if (self.i == 1):
                    # print("*****  state (9-1) branch  ******")
                    self.state = 1
                    self.turn = 1
                    return self.emitOne()

            """the branches when state=11"""

            if (self.turn == 0 and self.state == 11):
                self.i = self.inputx.next()
                # print("Input x2:", self.i)
                if (self.i) == -1:
                    self.state = 3
                    self.turn = 1
                    return self.emitZero()

                if (self.i == 0):
                    self.i = self.inputy.next()
                    # print("Input y2", self.i)
                    if (self.i == -1):
                        self.state = 11
                        return self.emitZero()
                    if (self.i == 0):
                        self.state = 4
                        return self.emitOne()
                    if (self.i == 1):
                        self.state = 5
                        return self.emitOne()
                if (self.i == 1):
                    self.state = 1
                    self.turn = 1
                    return self.emitOne()

            if (self.turn == 0 and self.state == 4):
                self.state=0
                return self.emitMinusOne()

            if (self.turn == 0 and self.state == 6):
                self.state=0
                return self.emitZero()

            if (self.turn == 0 and self.state == 8):
                self.state=0
                return self.emitZero()

            if (self.turn == 0 and self.state == 10):
                self.state=0
                return self.emitZero()

            if (self.turn == 0 and self.state == 12):
                self.state=0
                return self.emitOne()

def squareAverageStream(x,y):
  return AverageStream(sb.SquareStream(x),sb.SquareStream(y))

def doubleSquareAverageStream(x,y):
  return AverageStream(sb.SquareStream(sb.SquareStream(x)),sb.SquareStream(sb.SquareStream(y)))
