
# File: sb.py
# Author: John Longley
# Date: May 2017

# Basic machinery for signed binary streams
# Currently runs on Python 2.7.5 as on DICE machines

import dy
from dy import *

# 1,0,-1 as digits:
one = 1
zero = 0
minusOne = -1

# Infinite streams of these digits represent real numbers in the range [-1.0, 1.0]
# e.g. 1,0,0,0,0,... represents 0.5; -1,-1,-1,-1,... represents -1.0


class BasicStream:
  # plays role of 'abstract class' giving some basic operations for signed binary streams
  def __init__ (self):
    self.c = dy.zero # centre of current range
    self.r = 0       # - log2 (radius of current range), = number of digits output so far
    # So current range is from c-2^-r to c+2^-r.
  def centre (self):
    return self.c
  def radius (self):
    return self.r
  def max (self):
    return add (self.c, twoTo(-self.r))
  def min (self):
    return sub (self.c, twoTo(-self.r))
  def emitOne (self):
    self.r += 1 
    self.c = add (self.c, twoTo(-self.r))
    return one
  def emitZero (self):
    self.r += 1
    return zero
  def emitMinusOne (self):
    self.r += 1
    self.c = sub (self.c, twoTo(-self.r))
    return minusOne
  def next (self):   # dummy impl of 'next digit' method: overridden in subclasses
    return self.emitZero()
  def nextn (self, n):
    return [self.next() for i in range(n)]


class DyadicStream (BasicStream):
  # gives three ways of turning a given dyadic into a signed binary stream
  # e.g. 5/8 = 0.625 can be represented in signed binary by any of the following:
  #     1,0,0,1,1,1,1,1,...    1,0,1,0,0,0,0,0,...    1,1,0,-1,-1,-1,-1,-1,...
  # we can specify whether we want 1, 0 or -1 as the infinitely repeated "tail digit"
  def __init__ (self,dyadic,tailDigit):
    self.d = dyadic
    self.c = dy.zero 
    self.r = 0  # should find out how to call superclass initializer!
    self.t = tailDigit
  def emitOne (self):
    self.d = double (add (self.d, dy.minusHalf))
    return BasicStream.emitOne (self)
  def emitZero (self):
    self.d = double (self.d)
    return BasicStream.emitZero (self)
  def emitMinusOne (self):
    self.d = double (add (self.d, dy.half))
    return BasicStream.emitMinusOne (self)
  def next (self): # emits next digit and updates state
    if lt (dy.half, self.d): return self.emitOne()
    elif lt (self.d, dy.minusHalf): return self.emitMinusOne()
    elif self.t == one: # return smallest possible digit
      if lt (dy.zero, self.d): return self.emitZero()
      else: return self.emitMinusOne()
    elif self.t == minusOne: # return largest possible digit
      if lt (self.d, dy.zero): return self.emitZero()
      else: return self.emitOne()
    else: # t == zero
      if self.d == dy.half: return self.emitOne()
      elif self.d == dy.minusHalf: return self.emitMinusOne()
      else: return self.emitZero()

# For inspecting results:

def digitListToDyadic (l):
  s = dy.zero
  for i in range(len(l)):
    s = dy.add (s, dy.scale (dy.fromInt(l[i]), -(i+1)))
  return s

def digitListToFloat (l):
  return dy.toFloat (digitListToDyadic(l))

# Examples:

# bs = BasicStream()
# bs.nextn(8)        ## yields [0,0,0,0,0,0,0,0]
# 
# d = (5,3)          ## represents 5/8 = 0.625
# ds1 = DyadicStream(d,1)
# ds0 = DyadicStream(d,0)
# ds_ = DyadicStream(d,-1)
# 
# l1 = ds1.nextn(8)  ## [1,0,0,1,1,1,1,1]
# l0 = ds0.nextn(8)  ## [1,0,1,0,0,0,0,0]
# l_ = ds_.nextn(8)  ## [1,1,0,-1,-1,-1,-1,-1]
# 
# print (digitListToFloat (l1))  ## 0.62109375 
# print (digitListToFloat (l0))  ## 0.625
# print (digitListToFloat (l_))  ## 0.62890625


# First example of a function on real numbers: absolute value function

class AbsStream (BasicStream):
  def __init__ (self,input):
    self.c = dy.zero
    self.r = 0
    self.input = input
    self.sg = 0  # sign: set to 1 or -1 when first non-zero input digit is encountered
  def next (self):
    self.j = self.input.next()
    if self.j == zero: return self.emitZero()
    elif self.j == one:
      if self.sg == -1: return self.emitMinusOne()
      else:
        self.sg = 1
        return self.emitOne()
    else: # self.j == minusOne
      if self.sg == 1: return self.emitMinusOne()
      else:
        self.sg = -1
        return self.emitOne()

# Example:

# x0 = DyadicStream((-5,3),1)
# print x0.nextn(8)  ## gives [-1,-1,0,1,1,1,1,1]
# x1 = AbsStream (DyadicStream((-5,3),1))
# print x1.nextn(8)  ## gives [1,1,0,-1,-1,-1,-1,-1]


# Squaring function on signed binary streams

three = dy.fromInt(3)

class SquareStream (BasicStream):
  def __init__ (self,input):
    self.c = dy.zero
    self.r = 0
    self.input = AbsStream(input) 
       # helps if input >= 0, as squaring is then monotone
    self.sqmax = dy.one   # squares of current max/min input values,
    self.sqmin = dy.one   # relative to current output interval
                          # (N.B. square of min may not be min of square!)
  def processInputDigit (self):
    self.ic = self.input.centre()
    self.ir = self.input.radius()
    # so writing e = 2^-ir, current sqmin and sqmax are respectively
    # ic^2 - 2.ic.e + e^2 and ic^2 + 2.ic.e + e^2, 
    # or rather their residues within the current output interval
    self.j = self.input.next()
    if self.j == one:
       # update sqmin to be ic^2 (by incremental calculation)
       self.sqmin = dy.add (self.sqmin, dy.scale (self.ic, -self.ir+1+self.r))
       self.sqmin = dy.sub (self.sqmin, dy.twoTo (-self.ir*2+self.r))
    elif self.j == minusOne:
       # update sqmax to be inputc^2
       self.sqmax = dy.sub (self.sqmax, dy.scale (self.ic, -self.ir+1+self.r))
       self.sqmax = dy.sub (self.sqmax, dy.twoTo (-self.ir*2+self.r))
    else: # self.j == zero:
       # update sqmin to be inputc^2 - ic.e + e^2/4
       self.sqmin = dy.add (self.sqmin, dy.scale (self.ic, -self.ir+self.r))
       self.sqmin = dy.sub (self.sqmin, dy.scale (three, -self.ir*2-2+self.r))
       # update sqmax to be inputc^2 + ic.e + e^2/4
       self.sqmax = dy.sub (self.sqmax, dy.scale (self.ic, -self.ir+self.r))
       self.sqmax = dy.sub (self.sqmax, dy.scale (three, -self.ir*2-2+self.r))
  def emitOne (self):
    self.sqmin = double (add (self.sqmin, dy.minusHalf))
    self.sqmax = double (add (self.sqmax, dy.minusHalf))
    return BasicStream.emitOne (self)
  def emitZero (self):
    self.sqmin = double (self.sqmin)
    self.sqmax = double (self.sqmax)
    return BasicStream.emitZero (self)
  def emitMinusOne (self):
    self.sqmin = double (add (self.sqmin, dy.half))
    self.sqmax = double (add (self.sqmax, dy.half))
    return BasicStream.emitMinusOne (self)
  def next (self):
    # Case 1: input interval straddles 0, so min square is 0 rather than sqmin
    if lt (self.input.min(), dy.zero):
      if self.r == 0:
        return self.emitOne()  # first digit 1 comes for free
      # otherwise, 0 is lower limit of output range, so -1 the only possibility
      elif leq (self.sqmax, dy.zero):
        return self.emitMinusOne()
      else: # need more input info before we can emit
        self.processInputDigit()
        return self.next()
    # Case 2: sqmin is min of square
    elif leq (dy.zero, self.sqmin): 
      return self.emitOne()
    elif leq (self.sqmax, dy.zero): 
      return self.emitMinusOne()
    elif leq (dy.minusHalf, self.sqmin) and leq (self.sqmax, dy.half): 
      return self.emitZero()
    else: # need more input info before we can emit
      self.processInputDigit()
      return self.next() # recursion: will process as many input digits as necessary

# Examples:

# x0 = DyadicStream ((5,3),0)    ## represents 5/8
# y0 = SquareStream (x0)
# l0 = y0.nextn(15)
# print l0                       ## [1,0,-1,0,1,-1,0,0,0,0,0,0,0,0,0]
# print (digitListToFloat (l0))  ## 0.390625 = sq(0.625). No error as tail is all 0's.

# x_ = DyadicStream ((5,3),-1)   ## again represents 5/8
# y_ = SquareStream (x_)
# l_ = y_.nextn(15)
# print l_                       ## [1,0,0,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1]
# print (digitListToFloat (l_))  ## 0.390655517578, but with tail of -1's.
                                 ## For more precision, just request more binary digits!


# For more detailed testing: 'absolute' sqmin and sqmax

def rescale(y,k):
  return dy.halve (dy.add (dy.add (y.max(),y.min()),
                           dy.mult (k, dy.sub(y.max(),y.min()))))

def absoluteRange(y):
  return (rescale(y,y.sqmin), rescale(y,y.sqmax))



# Examples illustrating use of higher order functions

# def thrice (f,x):
#   return f(f(f(x)))

# thrice (lambda x: x+x, 5)   ## gives 40

# x = thrice (SquareStream, DyadicStream((5,3),1))
# dy.toFloat (digitListToDyadic (x.nextn(20)))  ## close approx to 0.625**8 = 0.0232830...

