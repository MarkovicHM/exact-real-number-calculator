
# File: my.py
# Author: Zhuyun Zhao
# Date: May 2017

# The implementation of 1-dimensional integration function 

import dy
from dy import *
import sb
from sb import *

def integrate(f,k):
  x=dy.minusOne
  area=dy.zero
  while x!=(dy.one): # calculate until x=-1
    s=DyadicStream(x,-1)
    t=f(s) # put the s into the function f
    new_digits=t.nextn(k+1) # should be within 2^-k
    y=digitListToDyadic(new_digits)
    x_next=s.max()
    area=dy.add(area,dy.mult(y,dy.sub(x_next, x))) # calculate the area (area+=y*(x_next-x))
    x=x_next
  print("The VALUE:",dy.toFloat(area))
  return area

def digitListToDyadic(l):
  numerator=0
  j=len(l)
  for i in list(l):
    if j>0:
      numerator += i*(2**(j-1))
    j=j-1
  return numerator,len(l)

  # Examples:

# integrate (sb.AbsStream, 10)
# integrate (sb.SquareStream, 18)
# print(dy.toFloat(b))
# """0-0.6666711121797562"""
# """-1-0.6666742786765099"""
##  returns (89479507, 27), value 0.66667427...: well within 2^-12 of exact answer 2/3
# integrate (lambda x: sb.SquareStream(sb.SquareStream(x)), 7)
##  returns (13171,15), value 0.401947...: within 2^-7 of exact answer 2/5

# (Can use dy.toFloat to get these floating-point representations).
# x=dy.minusOne
# s = DyadicStream(x, -1)
# print(dy.toFloat(s))
