
# File: dy.py
# Author: John Longley
# Date: May 2017

# Implementation of dyadic arithmetic, for use in exact real computation.
# Currently runs on Python 2.7.5 as on DICE machines

# Note that in python, integers are arbitrary-size by default.
# Multiplying/dividing by powers of 2 is done efficiently by bit-shifting.

# We represent a dyadic p * 2^-k (where k >= 0) by the pair (p,k).


# Simplifying a pair to lowest terms:
def simp (p,k):
  # common cases inlined for efficiency
  if p & 1 == 1 or k == 0 : return (p,k)
  elif p & 2 == 2 or k == 1 : return (p>>1, k-1)
  elif p & 4 == 4 or k == 2 : return (p>>2, k-2)
  elif p & 8 == 8 or k == 3 : return (p>>3, k-3)
  # otherwise recurse
  else : return simp (p>>4, k-4)

def neg (d):
  return (-d[0], d[1])

def abs (d):
  if d[0] >= 0: return d
  else: return neg(d)

def add (d,e):
  if d[1] >= e[1]: return simp (d[0] + (e[0] << (d[1]-e[1])), d[1])
  else: return simp ((d[0] << (e[1]-d[1])) + e[0], e[1])

def sub (d,e):
  return add(d, neg(e))

def mult (d,e):
  return simp (d[0]*e[0], d[1]+e[1])

def scale (d,k):  # d * 2^k
  if d[1] >= k: return simp (d[0], d[1]-k)
  else: return (d[0]*(1<<(k-d[1])), 0)
  # note 1<<n = 2**n

def double (d):
  return scale (d,1)

def halve (d):
  return scale (d,-1)

def truncate (d):  # truncates at 1 and -1
  m = 1<<d[1] # = 2**d[1]
  if d[0] >= m: return (1,0)
  elif d[0] <= -m: return (-1,0)
  else: return d

def lt (d,e):
  if d[1] >= e[1]: return d[0] < (e[0] << (d[1]-e[1]))
  else: return (d[0] << e[1]-d[1]) < e[0]

def gt (d,e):
  return lt (e,d)

def leq (d,e):
  return not lt (e,d)

def geq (d,e):
  return not lt (d,e)

def max (d,e):
  if lt (d,e): return e
  else: return d

def min (d,e):
  if lt (d,e): return d
  else: return e

def fromInt(n):
  return (n,0)

def twoTo(k):
  if k>0: return (1<<k, 0)
  else: return (1, -k)

def toFloat(d):
  return float(d[0]) / (1<<d[1])

def prin(d):
  print (toFloat(d))

# Some common dyadics:

one = (1,0) 
half = (1,1)
zero = (0,0)
minusHalf = (-1,1)
minusOne = (-1,0)

