import toR as r
import helper as h
import OOP as o
import math as m

def result(x):
	return h.output(o.BStoDS(x))

def inputFloat(a):
	return r.DtoBS(a)


#-----FOR FLOAT INPUT-----

#NOTE: cannot input negative numbers


def divByInt(a, i):
	x = r.DtoBS(a)
	return h.output(o.BStoDS(r.divByIntR(x, i)))

def multByInt(a, i):
	x = r.DtoBS(a)
	return h.output(o.BStoDS(r.multByIntR(x, i)))

#absolute value of mult
def mult(a, b):
	x = r.DtoBS(a)
	y = r.DtoBS(b)
	return h.output(o.BStoDS(r.multR(x, y)))

#negative a will give same result as a (because it uses mult)
def exp(a):
	x = r.DtoBS(a)
	return h.output(o.BStoDS(r.expR(x)))

def add(a, b):
	x = r.DtoBS(a)
	y = r.DtoBS(b)
	return h.output(o.BStoDS(r.addR(x, y)))

def sub(a, b):
	x = r.DtoBS(a)
	y = r.DtoBS(b)
	return h.output(o.BStoDS(r.subR(x, y)))

def average(a, b):
	x = r.DtoBS(a)
	y = r.DtoBS(b)
	return h.output(o.BStoDS(r.averageR(x, y)))

def square(a):
	x = r.DtoBS(a)
	return h.output(o.BStoDS(r.squareR(x)))

#positive since it uses mult
def div(a, b):
	x = r.DtoBS(a)
	y = r.DtoBS(b)
	return h.output(o.BStoDS(r.divR(x, y)))

#-----FOR STREAM INPUT------

def divByIntS(x, i):
	return h.output(o.BStoDS(r.divByIntR(x, i)))

def multByIntS(x, i):
	return h.output(o.BStoDS(r.multByIntR(x, i)))

#will be absolute value
def multS(x, y):
	return h.output(o.BStoDS(r.multR(x, y)))

# uses multiplication therefore it'll use the absolute value of x
def expS(x):
	return h.output(o.BStoDS(r.expR(x)))

def addS(x, y):
	return h.output(o.BStoDS(r.addR(x, y)))

def subS(x, y):
	return h.output(o.BStoDS(r.subR(x, y)))

def averageS(x, y):
	return h.output(o.BStoDS(r.averageR(x, y)))

def squareS(x):
	return h.output(o.BStoDS(r.squareR(x)))

def absS(x):
	return h.output(o.BStoDS(r.absR(x)))

#also uses multiplication therefore will be absolute value
def divS(x, y):
	return h.output(o.BStoDS(r.divR(x, y)))















