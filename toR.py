import OOP as o
import functional as f
import external.average as av
import helper as h
import higher_ops as ho
import external.sb as sb
import BS_Factory as fact
import math as m
import alternate as alt

def divByIntR(x,i):
	return (o.divByInt2(x[0],i),x[1])

def multByIntR(x, i):
	if(i<0):
		return (h.neg(f.multByInt(x[0], int(m.fabs(i)))), x[1] + int(m.ceil(m.log(m.fabs(i),2))) + 1)
	else:
		return (f.multByInt(x[0],i), x[1] + int(m.ceil(m.log(m.fabs(i),2))) + 1)

def addR(x,y):
	av = averageR(x,y)
	return (av[0], av[1] + 1)

def negR(x):
	return (h.neg(x[0]),x[1])

def subR(x,y):
	return addR(x, negR(y))

def averageR(x,y):
	newy = 0
	newx = 0
	if (x[1] > y[1]):
		newy = h.shiftRightBy(y[0],x[1]-y[1])
		average = av.AverageStream(newy,x[0])
		return (average, x[1])

	elif(y[1] > x[1]):
		newx = h.shiftRightBy(x[0],y[1]-x[1])
		average = av.AverageStream(y[0],newx)
		return (average, y[1])

	else:
		average = av.AverageStream(y[0],x[0])
		return (average, y[1])

def multR(x,y):
	return (alt.multStream(x[0],y[0]), x[1]+y[1])

def squareR(x):
	return (sb.SquareStream(x[0]),2*x[1])

def absR(x):
	return (sb.AbsStream(x[0]),x[1])

def expR(x):
	i = x[1]
	result = (ho.exp(x[0]), 2)
	while(i > 0):
		result = squareR(result)
		i -= 1
	return result

def divR(x,y):
	inv = o.inverse(y)
	inv.initialise()
	invE = inv.exp
	return (alt.multStream(x[0],inv),x[1]-invE)

def DtoBS(x):
	a = h.floatToStr(m.fabs(x))
	b = o.DStoBS(a[0])
	if(x<0):
		b = h.neg(b)
	if(a[1]>=0):
		return multByIntR((b,0), 10**a[1])
	else:
		return (b,0)

def simp(x):
	xFact = fact.BS_Factory(x[0])
	count = 0
	new = xFact.copy()
	small = True
	while(small):
		if(new.next() == 0 and x[1]-count > 0):
			count += 1
		else:
			small = False
	return (h.shiftLeftBy(xFact.copy(), count), x[1]-count)












#(e^3/2^4)^2^4*2^-2
# 0.11 * 2^4 + 0.2 * 2^2
