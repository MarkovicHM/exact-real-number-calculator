import BS_Factory as fact
import external.average as av
import external.sb as sb
import math as m
import demo
import dec

#formats output
class output(dec.DecimalStream):
	def __init__(self,stream):
		self.stream = stream
		self.dlist = fact.DList()
		self.p = 0
		self.exp = stream.log
	def next(self):
		d = self.stream.next()
		self.dlist.addD(d)
		self.p+=1
		return d

	def nextn(self, n):
		[self.next() for i in range(n)]
		a, ng = demo.unSign(self.dlist)
		return format(a, self.exp, ng)

#turns list into number (taking into account the exponent)
def format(x, e, ng):
	count = 0
	if(ng):
		num = '-'
	else:
		num = ''
	start = True
	while(count<len(x)):
		if(start):
			while(x[count] == 0 and e > 0):
				count += 1
				e -= 1
			start = False
		if(e == 0):
			e -= 1
			num += '.'
		else:
			e -= 1
			num += str(x[count])
			count += 1
	return num

#float to stream
def floatToStr(x):
	ngt = False
	#check if the input is negative 
	if(x<0):
		ngt = True
		exp = int(m.ceil(m.log(-x,10)))
		x = -x
	else:
		exp = int(m.ceil(m.log(x,10)))
	l = str(x)
	l = l.replace('.','')
	if(l[0] == '0'):
		l = l[1:]
	#if the input was negtive we negate the stream
	if(ngt):
		if(exp >= 0):
			return (neg(strToBS(l)), exp)
		else:
			return (neg(strToBS(l)), 0)
	else:
		if(exp >= 0):
			return (strToBS(l), exp)
		else:
			return (strToBS(l), 0)

#average with one
class avW1(sb.BasicStream):
	def __init__(self, x):
		sb.BasicStream.__init__(self)
		self.x = x
		self.isFirst = True
	def next(self):
		if (self.isFirst):
			self.isFirst = False
			return self.emitOne()
		else:
			return self.x.next()

#finite dec stream to number
def toNum(s, d):
	exp = s.log
	lis = s.nextn(d)
	i = 1
	num = 0
	for n in lis:
		num += n*10**(exp-i)
		i += 1
	return num

#turns integer from decimal to binary
def digToB1(d):
	return "{0:b}".format(d)

#turns integer into formatted binary (4 digits)
#first line found at https://stackoverflow.com/questions/699866/python-int-to-binary
def digToB(d):
	b = "{0:b}".format(d)
	while (len(b)<4):
		b = '0' + b
	return b

#turns double digit into formatted binary (7 digits)
def doubleDigToB(d):
	b = "{0:b}".format(d)
	while (len(b)<7):
		b = '0' + b
	return b

#turns integer into binary stream (4 significant digits)
class digToBS_s(sb.BasicStream):
	def __init__(self,decimal):
		sb.BasicStream.__init__(self)
		self.d = decimal
		self.rest = digToB(decimal)
		self.count = 4

	def next(self):
		if(self.count > 0):
			a = self.rest[0]
			self.rest = self.rest[1:]
			self.count -= 1
			if (int(a) == 1):
				return self.emitOne()
			else:
				return self.emitZero()
		else:
			return self.emitZero()

#turns integer into binary stream (7 significant digits)
class digToBS(sb.BasicStream):
	def __init__(self,decimal):
		sb.BasicStream.__init__(self)
		self.d = decimal
		self.rest = doubleDigToB(decimal)
		self.count = 7

	def next(self):
		if(self.count > 0):
			a = self.rest[0]
			self.rest = self.rest[1:]
			self.count -= 1
			if (int(a) == 1):
				return self.emitOne()
			else:
				return self.emitZero()
		else:
			return self.emitZero()

#turns string into binary stream
class strToBS(sb.BasicStream):
	def __init__(self,decimal):
		sb.BasicStream.__init__(self)
		self.rest = decimal
		self.count = len(decimal)

	def next(self):
		if(self.count > 0):
			a = self.rest[0]
			self.rest = self.rest[1:]
			self.count -= 1
			return int(a)
		else:
			return self.emitZero()

def add(s1,s2):
	ave = av.AverageStream(s1,s2)
	newS = fact.BS_Factory(ave)
	if (newS.copy().next() == 0):
		return shiftLeftBy(newS.copy(),1)
	else:
		raise Exception('cannot add, number too large')

def shiftLeftBy(input, x):
	a = input.nextn(x)
	if(not all([b == 0 for b in a])):
		print a
		print 'losing information'
	return input

class shiftRightBy(sb.BasicStream):
	def __init__(self,stream, x):
		sb.BasicStream.__init__(self)
		self.stream = stream
		self.x = x
	def next(self):
		if (self.x>0):
			self.x -= 1
			return self.emitZero()
		else:
			return self.stream.next()

class neg(sb.BasicStream):
	def __init__(self,stream):
		sb.BasicStream.__init__(self)
		self.stream = stream
	def next(self):
		a = self.stream.next()
		return -a

def sub(s1,s2):
	s2_n = neg(s2)
	return add(s1,s2_n)

class oneMinus(sb.BasicStream):
	def __init__(self,x):
		sb.BasicStream.__init__(self)
		self.x = x
		self.first = True
		self.second = False
		self.one = False

	def next(self):
		if(self.first):
			self.first = False
			self.second = True
			return self.emitOne()

		elif(self.second):
			self.second = False
			if(self.x.next() == 1):
				self.one = True
				return self.next()
			else:
				return self.next()
		else:
			if(not self.one):
				if(self.x.next() == 0):
					return self.emitOne()
				else:
					self.one = True
					return self.emitOne()
			else:
				a = self.x.next()
				if(a == -1):
					return self.emitOne()
				elif(a == 0):
					return self.emitZero()
				else:
					return self.emitMinusOne() 

	

#turns decimal sample (e.g. 123) into infinitely repeating 
#decimal stream (eg. 123123123123...)
class expDecS(dec.DecimalStream):
	def __init__(self, sample):
		dec.DecimalStream.__init__(self)
		self.sample = sample
		self.counter = -1

	#iterates the counter along the digits of the output then
	#loops back to the beginning
	def count(self):
		self.counter += 1
		self.counter = self.counter%len(self.sample)

	#emits the next digit in line
	def next(self):
		self.count()
		return self.emit(int(self.sample[self.counter]))




	











