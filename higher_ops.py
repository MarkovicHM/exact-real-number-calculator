import OOP as o
import external.sb as sb
import BS_Factory as fact
import helper as h
import external.average as av
import alternate as alt


def exp(x):
	xfact = fact.BS_Factory(x)
	# .10000...
	half = h.digToBS_s(8)
	return av.AverageStream(half, o.multStream(xfact.copy(),exprec(xfact,2)))

class exprec(sb.BasicStream):
	def __init__(self, xfact, n):
		sb.BasicStream.__init__(self)
		self.xfact = xfact
		self.isFirst = True
		self.started = False
		self.n = n
		self.result = 0

	def next(self):
		if(self.isFirst):
			self.isFirst = False
			#   average with 1
			return self.emitOne()
		else:
			if(not self.started):
				new = exprec(self.xfact,self.n+1)
				#  xfact * exprec(xfact,n+1)
				mult = o.multStream(self.xfact.copy(), new)
				#  (xfact * exprec(xfact,n+1)) / n
				divI = o.divByInt1(mult,self.n)
				#  2 * (xfact * exprec(xfact,n+1)) / n
				self.result = h.shiftLeftBy(divI,1)
				self.started = True
				return self.result.next()

			else:
				return self.result.next()

def inverse(y):
	# x = (1-y)/2
	x = h.shiftRightBy(h.oneMinus(y),1)
	xfact = fact.BS_Factory(x)
	#print xfact.copy().nextn(10)
	times2 = h.shiftLeftBy(xfact.copy(),1)
	# 1 av 2x(1 av 2x(1 av 2x(...)))
	#return h.avW1(alt.multStream(x, invrec(xfact)))
	return h.avW1(alt.multStream(times2, invrec(xfact)))


class invrec(sb.BasicStream):
	def __init__(self, xfact):
		sb.BasicStream.__init__(self)
		self.xfact = xfact
		self.isFirst = True
		self.started = False
		self.result = 0

	def next(self):
		if(self.isFirst):
			self.isFirst = False
			#average with one
			return self.emitOne()
		else:
			if(not self.started):
				new = invrec(self.xfact)
				# 1 av x * invrec(x)
				#mult = alt.multStream(self.xfact.copy(), new)
				self.result = alt.multStream(h.shiftLeftBy(self.xfact.copy(),1), new)
				# 1 av 2x * invrec(x)
				#self.result = h.shiftLeftBy(mult,1)
				self.started = True
				return self.result.next()

			else:
				return self.result.next()




