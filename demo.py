import toR as r
import helper as h
import OOP as o

def divByInt(a, i, num = 0):
	x = h.digToBS_s(a)
	if (num == 0):
		return o.BStoDS(r.divByIntR((x,4), i))
	else:
		return h.toNum(o.BStoDS(r.divByIntR((x,4), i)),num)

def multByInt(a, i, num = 0):
	x = h.digToBS_s(a)
	if (num == 0):
		return o.BStoDS(r.multByIntR((x,4), i))
	else:
		return h.toNum(o.BStoDS(r.multByIntR((x,4), i)),num)

def mult(a, y, num = 0):
	x = h.digToBS_s(a)
	if (num == 0):
		return o.BStoDS(r.multR((x,4), (y,4)))
	else:
		return h.toNum(o.BStoDS(r.multR((x,4), (y,4))),num)

def exp(a, num = 0):
	x = h.digToBS_s(a)
	if (num == 0):
		return o.BStoDS(r.expR((x,4)))
	else:
		return h.toNum(o.BStoDS(r.expR((x,4))),num)

def unSign(y):
	j = 0
	k = 0
	a = y.len()
	i = 1
	x = y.returnDList()
	ng = False
	
	while(j<a and x[j] == 0):
		j += 1
	if(j<a and x[j]<0):
		ng = True
	if(ng):
		while(k<a):
			x[k] = -x[k]
			k +=1
	while(i<a):
		if (x[a-i]<0):
			x[a-i-1] = x[a-i-1]-1
			x[a-i] = 10+x[a-i]
		i+=1
	return x, ng
		

